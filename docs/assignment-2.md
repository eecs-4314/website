## Concrete Architecture

[Presentation Slides](https://docs.google.com/presentation/d/1a3h3s_7Xj6kx_Td1hJSfloL56UKAVNRj41p3LNxY2lo/edit?usp=sharing)

[Report (PDF)](https://drive.google.com/file/d/1GQNbU5HJ_J_E02E2tnT8Kfp1Ok-NlKII/view?usp=sharing)



1 Abstract
==========

This report presents an overview of the concrete architecture of the User Interface subsystem of IntelliJ Platform. An introduction and overview of its high level architecture is presented as well as its derivation process. A discussion on the use of the dependencies, comparison with the conceptual architecture from Assignment 1, and a reflexion analysis of each major subsystem component is provided. A few use cases are provided to present the interaction between the UI subsystem and other top-level subsystems. Lastly, a conclusion and lessons learned during the creation of the concrete architecture is also given.

2 Introduction
==============

IntelliJ IDEA is a complex open source integrated development environment which provides many useful features for developers. On a high-level architecture, IntelliJ’s is divided into 5 main subsystems: Core, Programming Structure Interface (PSI), Plugins, Virtual File System (VFS), User Interface (UI), Application, and Project Model. This report examines the UI subsystem by looking at the conceptual and concrete architecture in conjunction to high-level subsystems.

2.1 Conceptual Architecture
---------------------------

![](a2-assets/images/image7.png)
----------------------

Figure 1: Conceptual Architecture

![](a2-assets/images/image11.png)
-----------------------

Figure 2: Concrete Architecture

2.2 Subsystem: User Interface
-----------------------------

User Interface is an important component of an application because it allows the user to interact with the application. In IntelliJ IDEA, basic usability and functionality is provided by the User Interface. In this section we discuss the important low-level subsystems that make the User Interface system. Listed below are the subsystems of UI and their interactions with other systems:

*   Editor is composed of a window in which the user can view/modify files and code. Editor uses the Notification system to display and modify any changes to the file. Editor also uses File and Class Chooser systems to display the contents of a file/class. Lastly, Editor depends on a higher level application subsystem in order to reflect changes made to a file, which the VFS uses for snapshots on an application level scope.
*   Notifications subsystem is used to coordinate different subsystems of UI and provide synchronization to other higher-level subsystems in conjunction. Notifications allow the user to be notified of any errors that occured in the editor window. In addition, the user can invoke certain actions throughout the user interface which get processed by the Notification subsystem and delegated to corresponding system(s).
*   Action System is used to perform an action(s) upon invocation. An easy example of this would be the compile button in the toolbar, when pressed would tell the system to compile the code in current open file. Action System has a strong bi-directional dependency on plugin subsystem because actions can be created and registered as part of a plugin. Since there are many actions each with different behaviour(s), all UI subsystems have a dependency on it. In addition, any operation that require and action, depends on the core for concurrency restrictions.
*   List and Tree Controls provides a structure and format for displaying text, icons and images. Editor, Tool Window, Dialogs, Popups, and Icon subsystems use the List and Tree Control subsystem for displaying objects in a structured and formatted manner.
*   Dialogs are small windows that are displayed to the user, showing information and may ask for user input (uses Action System). Dialogs are used by all low-level subsystems of UI because these dialogs can be displayed in tool window, file and class chooser, editor, and popups. On the other hand, Dialogs have a high-level dependency on the Core (due to messages and threading restrictions), plugins (for implementing various types of dialogs), and Virtual File System (for displaying file/directory information).
*   Tool Windows are used to displaying information (such as project files) on the window, located on the ends of editor view. A new tool window can be created using a plugin. Tool Windows provide a perspective view which can display files and classes (dependency on VFS, Project Model, Application, Icons, PSI). Files can be selected for viewing, hence the Editor is used in conjunction with Action System.
*   Popups: IntelliJ makes extensive use of popups across the User Interface. Popups appear upon use invocation, and/or message bus (core and Notification). Popups mainly occur in the Editor and Tool windows. Some functionality of Popup functions require traversing of PSI tree to display appropriate information.
*   File and Class Choosers lets a user view/use a file(s) or class(es). This is mainly done via getting ijonformation form VFS, PSI, while using Core (concurrent operations). Action System, Tool Windows, and List and Tree Control are also used extensively by this subsystem in order to display the files fetched from VFS and PSI.
*   Miscellaneous classifies all other UI subsystems which are not abundant or as important as the subsystems listed above. Our rationale behind this is that UI is not a decoupled system. In order to get a readable/understandable LSEdit, we had to classify the remaining smaller UI subsystems into a Miscellaneous subsystem.  

### 2.2.1 Under the hood

IntelliJ is a cross platform software i.e. is able to run on MacOS, Linux and Windows operating system. It runs on top of Java Virtual Machine (JVM) and requires to interact with OS APIs. Under the hood IntelliJ Platform uses Swing to create its Graphical User Interface (GUI). Most of UI components offered by Swing that IntelliJ uses are based on Java AWT. Java AWT calls native platform (Operating systems) subroutine for creating components such as textbox, checkbox, button etc. For example an AWT GUI having a button would have a different look and feel across platforms like Windows, Mac OS & Linux; this is because these platforms have different look and feel for their native buttons and AWT directly calls their native subroutine that creates the button.

![](a2-assets/images/image8.png)

Figure 3: Layered architecture of IntelliJ interactions with host OS

IntelliJ created certain UI components, events and actions on top of Swing which are specific to the IDE itself and made them available for core and plugin developers. These components reside in openapipackages and are part of the DevKit used for creating plugins and extending the platform. We shall see this in the Use Case section.

3 Architecture Derivation and Recovery Process
==============================================

Normally the concrete architecture process starts with fact extraction. However, fact extraction was already done giving us the dependencies in the form of a tuple-attribute-file. The tuple-attribute-file was updated with a named subsystem from the conceptual architecture. Then Java files were selected and grouped under the subsystem based on file naming, directory structure or Jetbrains documentation on the IntelliJ platform. The documentation provided relevant Java files and their locations for its notable components. These components were also our subsystems. Bash scripts were used to find the Java files in certain directories, written and formatted to the containment file under its subsystem and then visualized in LSEdit.

![](a2-assets/images/image5.png)

3.1 Top Level Derivation
------------------------

![](a2-assets/images/image2.png)

Figure 4: The top level concrete architecture in LSEdit

Derivation of the top-level systems mostly relied on directory names matching the subsystem names. Typically, directories contained the same name as our top-level subsystem names. The search for files within our subsystems was limited to the platform directory as the IntelliJ Platform guide was used to name our subsystems. One exception to this was the Plugins subsystem. In the documentation, Plugins extended classes such as ServiceManager and Application contained in a component directory. Files in component directories were assigned to the Plugins subsystem. Directory searching was mostly manual and files may have been missed. If a directory was not searched deeply or named differently from our subsystem it may have not been in the containment.

3.2 UI Subsystems Derivation
----------------------------

![](a2-assets/images/image10.png)

Figure 5: The concrete architecture of the subsystems within UI in LSEdit

Derivation of the subsystems within UI could not rely on directory names the same way as derivation for the top-level systems did. Most directory names did not match up with the notable UI components mentioned in IntelliJ’s platform guide. Many of the Java files referenced in the guide were to files in directories other Java UI files were found in.

To distinguish what file belonged to what subsystem the name of the file was used. Shell scripts searched within the files belonging to the UI subsystem for names containing our UI subsystem names. Often conflicts happened for the containment file. Typically conflicts shared two subsystem names such as EditorNotification with the Editor and Notification subsystem. There was one extreme case of a file named ShowNotificationIconsDialogAction which shared names with four of our subsystems. To decide for these cases, the scripts were given priorities for certain subsystems. Files containing “Notification” in it went into the Notification subsystem over the Editor subsystem in the case of EditorNotification. Many files still went without going into a proper subsystem and were grouped into the Miscellaneous subsystem in the end.

4 Differences Between Conceptual & Concrete Architecture
=======================================================

The conceptual architecture of the overall system is depicted in Figure 1. With respect to the UI, in the first concept of the architecture the UI was considered to be dependant on the core alone. This was the prevailing theory as we had assumed that the developers would want to decouple the UI from the other systems and localize the calls the UI would have to make to just the core system. This made the most sense because then there would be much less for future additions as this would create a centralized location for all UI features. It was also conceptualized that the Application and Plugin layers would be dependant on the UI to allow for the application to update based on changes in the UI and for plugin developers to be able to watch changes represented in the UI.

After deriving the concrete architecture we found that we had been incorrect about the UI’s dependency. The biggest difference between the concrete and conceptual architecture in respect to UI is that it depends on all systems. Where we assumed that the developers would go for an abstract approach the developers instead opted for a more direct approach. With these dependencies we can conclude that the developers are directly observing every layer from the UI and with the use of an observer pattern the UI can immediately respond to changes in each layer. This of course comes with some drawbacks as it becomes more and more difficult to track what makes what changes in the UI. The concrete architecture also shows that the VFS, the application, and plugin layers are dependant on the UI. these layers use the UI to track changes that the user is making. The Virtual File System watches for changes the user makes to the graphical representation of the file system and makes changes accordingly. The Plugin layer watches for users to click on features provided by the plugin and the application layer watches for everything else. When a user types into the user interface the application layer watches and updates the corresponding layers accordingly.

For subsystems within UI, there are more dependencies than expected creating a tightly coupled system compared to the conceptual architecture of UI. The reason for this comes from the coverage UI has. The amount of files found belonging to UI is more than 600, compared to the approximately 500 files found for Core or approximately 400 files found for PSI. This increases the chances that UI would have dependencies with other files.

Another reason for these many dependencies, UI could have many more subsystems, but still would have UI files not fit into these subsystems because of them building off of many different UI files. Files like EditorNotification and ActionPopupMenu are built from related subsystems and are difficult to group into one subsystem. Many UI components build on each other tightening their coupling with each other.

4.1 Reflexion Analysis
----------------------

### 4.1.1  Reasoning behind PSI dependency on UI

Using Sticky Note method, we looked at the commit graph and found commit [e769068f5c07](https://www.google.com/url?q=https://github.com/JetBrains/intellij-community/commit/e769068f5c07aa8fbbf9c6d59d1cafd372200f09&sa=D&ust=1572892994414000) using the Git Blame feature. We inspected the entire project before and after this change to see what was changed. According to our findings, there was an effort taken by a lead engineer to decouple the ui package from other packages; this involved extracting code and creation of new interfaces. However some classes required big refactors and thus not touched at all resulting in reference to ui subsystem. LSEdit is not that smart to understand the intricacies of real world software so we had to manually analyze the cause.

**Which low level code entity is responsible for the dependency?**

com.intellij.ui.content.impl.ContentManagerImpl

**Who added/removed the dependency?**

Alexey Kudravtsev

13,015 commits  1,209,920 ++ 957,924 --

**When was the dependency modified?**

Dec 16, 2018

**Why was the dependency added/removed?**

dispose FileManager after Usage view because the latter needs the former during its disposal

### 4.1.2  Reasoning behind Plugins dependency on UI

Inspecting the code in the plugin we found the dependency of the plugin component on the UI component. We found this dependency in the byteCodeViewer class. The dependency is on the editor component of the UI system. The reason for this dependency is obvious as editor is used to display and edit code/text. This dependency was added on December 28th, 2012. This dependency has persisted until present day. Last commit to this class was a year ago. The person to add this dependency is Anna who seems like a senior developer as further investigation of other classes revealed her to be the author of those classes as well. It seems she worked extensively on this project and on important classes.

**Which low level code entity is responsible for the dependency?**

com.intellij.byteCodeViewer

(plugins/ByteCodeViewer/src/com/intellij/bytecodeviewer)

**Who added/removed the dependency?**

Anna

**When was the dependency modified?**

Dec 28, 2012 (The dependency follows through to present day. The last commit was a year ago to this plugin)

**Why was the dependency added/removed?**

The bytecode viewer plugin depends on the editor component from UI to display/set the bytecode. The dependency is quite obvious as editor is used to display and set code/text in the IDE.

### 4.1.3  Reasoning behind VFS dependency on UI

Inspecting the dependency graph, we found that that VFS is dependent upon UI. The dependency is focused mostly in the vfs encoding system of vfs. Looking through the git history of this system which is located mostly the same folder, and was added on the same day, Feb 15,2013 by a senior developer of the project. The commit history did not mention about this addition of dependency, however as our group read through the code, we concluded that it was to add UI component to the vfs system. More specifically it was messages box for what the system was doing as well as options for users to use the vfs system. Most of these changes still exists in the source code until this day.

**Which low level code entity is responsible for the dependency?**

com.intellij.openapi.vfs.encoding

**Who added/removed the dependency?**

Alexey Kudravtsev

13,015 commits  1,209,920 ++ 957,924 --

**When was the dependency modified?**

Feb 15, 2013 (The dependency follows through to present day.)

**Why was the dependency added/removed?**

The dependency was added to add a UI component for the encoding process

5 Architecture Styles & Patterns
================================

5.1 Adapter Pattern
-------------------

![](a2-assets/images/image3.png)

Figure 6: Adapter Pattern Structure

IntelliJ support multiple threading so it has many listeners which are inherited from EventListener interface. In UI subsystem, notification plays an important role. Then, DocumentListener interface is used to send notifications when documents have been changed. DocumentEvent interface is used to record all detailed information of how the document changed. However, DocumentEvent interface cannot be used as parameter of DocumentListener interface directly. Therefore, the DocumentAdapter class is used to change the document events and let them can be sent as parameters to DocumentListener interface.

5.2 Facade Pattern
------------------

![](a2-assets/images/image9.png)

Figure 7: Facade Pattern Structure

Facade pattern hides the complexities of the system and provides an interface to the client using which the client can access the system. The FacetEditorFacade interface provides the main structure of facet editor. Therefore, clients also can use different classes which implement FacetEditorFacade interface based on their domain. Facade Pattern is widely used in refactoring, a new API is created by defining an interface which wraps the older APIs within itself thus abstracting out the inner workings.

5.3 Event Listener Pattern
--------------------------

Event-driven architecture is a software architecture paradigm promoting the production, detection, consumption of, and reaction to events. An event can be defined as “a significant change in state”. Event-driven architecture is widely seen in UI and is made possible by Event Listeners. For example, a listener can be defined for a button on a user interface. If the button is selected, the listener is notified and performs a certain action. In the following code snippet from IntelliJ source, a listener is added to a popup element and a custom behaviour is defined when popup is closed. Consider the example below:

```java
popup.addListener(newJBPopupListener(){  
   @Override  
   publicvoidonClosed(@NotNullLightweightWindowEventevent){ myCanClose\=true;} });  
})
```


5.4 Builder Pattern
-------------------

Builder pattern is used when there is a number of parameters for some constructor. The builder pattern provides a step by step creation of complex objects thus making the API fluent. In software engineering, a fluent interface (as first coined by Eric Evans and Martin Fowler) is a method for designing object oriented APIs based extensively on method chaining with the goal of making the readability of the source code close to that of ordinary written prose, essentially creating a domain-specific language within the interface.
```java
JBPopupFactory.getInstance().createPopupChooserBuilder(messages)  
     .setFont(...)  
     .setVisibleRowCount(...)  
     .build()
```
5.5 Abstract Factory Pattern
----------------------------

Abstract Factory is a creational design pattern, which solves the problem of creating entire product families without specifying their concrete classes. One instance where the abstract factory pattern was used is JBPopupFactor, which is a Factory class for creating popup chooser windows and various notifications/confirmations.

![](a2-assets/images/image6.png)

Figure 8: Lightweight tooltip (a type of popup)

JBPopupFactoryjb\=JBPopupFactory.getInstance();

5.6 Model View Controller (MVC) Pattern
---------------------------------------

EditorWindow.java
```java
myOwner.updateFileIconImmediately(file);

publicvoidupdateFileIcon(@NotNullfinalVirtualFilefile){
    updateFileIconLater(file);
}
```
EditorsSplitters.java
```java
voidupdateFileIconImmediately(finalVirtualFilefile){
    finalCollection<EditorWindow\>windows\=findWindows(file);

    for(EditorWindowwindow:windows){
        window.updateFileIcon(file);
    } 
}
```

Observer synchronization is the central part of Model View Controller pattern. And the purpose of observer synchronization is when a change of  screens has been made, then all the others should be changed as well. As above code shows, when the icon of the file has been changed, all other editor windows will update this change immediately.

6 Example use case
==================

6.1 UI of a plugin
------------------

Plugin functionality for IntelliJ typically falls into one of 4 categories:

1.  Custom languages: the ability to write, interpret, and compile code written in different languages
2.  Frameworks: support for 3rd party frameworks such as Spring
3.  Tools: integration with external tools such as Gradle
4.  User interface add-ons: new menu items, tool windows and buttons, and more

Plugins will often fall into multiple categories. For example, the Git plugin that ships with IntelliJ, interacts with the git executable installed on the system. The plugin provides its tool window and popup menu items, while also integrating into the project creation workflow, preferences window, and more.

Actions are the core component used for writing IntelliJ plugins. Actions get triggered by events in the IDE, such as clicking a menu item or toolbar button.

In our example[\[1\]](#ftnt1)plugin we perform a StackOverflow search of selected text by the user. This involves creating an Action that opens the browser. We use BrowserUtil class because it handles all the nuances of opening a web page on different operating systems and browsers. Complete source code[\[2\]](#ftnt2) is available
```
public class AskQuestionAction extends AnAction {

@Override
public void actionPerformed(AnActionEvente) {
        BrowserUtil.browse("[https://stackoverflow.com/questions/ask](https://www.google.com/url?q=https://stackoverflow.com/questions/ask&sa=D&ust=1572892994449000)";
    }
}

ActionManager.getInstance().registerAction( "StackOverflow.SearchAction", newSearchAction());
```

![](a2-assets/images/image4.png)

6.2 ByteCodeViewerComponent
---------------------------

![](a2-assets/images/image1.jpg)

Figure 9: ByteCodeViewComponent sequence diagram

The above sequence diagram shows the sequence of steps needed to set text in the editor of the IDE. The byteCodeViewerComponent class has to first make an editor object. To do this it interacts with the editor class which is a UI component. After the object is made the setText() method is used in the byteCodeViewerComponent class. This in turn calls the getDocument() method in the editor class which in turn calls getInstance() method of the document class. The FileDocumentManager returns the instance to the Document class which in turn returns the Document setting the text in it and then displaying it on the editor.  

7 Concurrency
=============

IntelliJ’s User Interface is mainly an event driven, hence the Notifications subsystem of UI uses the threading and message (part of the Core) subsystems to keep all systems in sync via handling and raising events. The threading system allows UI subsystem to preserve a write-lock in the UI thread which is unique only to the UI subsystem. In comparison, read-lock is preserved by all subsystems of IntelliJ.EditorNotificationsImp.Java, a platform UI class is one example where concurrency is used to handle multiple write operations to other classes in order to achieve synchronization on a project level scope.

8 Conclusion
============

The IntelliJ Platform is a complex platform spanning many systems and subsystems. The User Interface (UI) is made up of several subsystems including; the Editor, Notifications, Action System, List and Tree Controls, Dialogs, Tool Windows, Popups, File and Class Choosers, and several other smaller subsystems. Each of these subsystems and many others are dependant on each other and systems outside of the UI subsystem. These subsystems and their interactions were derived through a series of shell scripts and through the use of LSEdit. Top level derivation revealed that our initial conceptual architecture was incorrect and a new concrete architecture was established. Deeper derivations into the UI subsystem became difficult to concretely define as directory names did not match with notable UI subsystems. Clear subsystems were soon defined using file names. Using a Sticky Note method for reflexion analysis we derived the reason for several different dependencies on the UI system. It was found that efforts have been mounted to decouple UI from other systems but the task proved too great and as such, several systems continue to depend on the UI. The UI system employs several different patterns including, the Event Listener Pattern,the Builder Pattern, the Abstract Factory pattern as well as the MVC Pattern.

9 Lessons Learned
=================

Throughout this project we have faced a few hurdles. Specifically during the derivation process we had struggled with how one derives an architecture from a system. Once we began using LSedit we quickly learned that there is more work required than just plugging in a system and running the program. With careful planning and some skilful shell scripting our resident LSedit wizard guided us through this process. We were of course stifled by the fact that documentation for the deep subsystem’s we derived does not exist, on top of that, commit messages were short and usually indescriptive. We learnt a lot during this process though there is so much more to learn and understand.

Furthermore, during our conversations about the differences between our conceptual and concrete architecture we learned very quickly that we were incorrect in properly defining the architecture the first time around. At first we thought that we had made some severe mistakes, however, we took that as a learning experience. We pointed out the areas that we were wrong about and discussed how and why the differences existed. This gave us an insight of how we would design this architecture and we were able to compare that to the real architecture. It was fascinating to see how theoretical designs give way to practical designs. We took note and came to the conclusion that the systems must have been built over a long period of time and by many different developers and as such they made many decisions that may not have been considered when the project was first being developed. This makes a lot of sense when one considers how interconnected all of the top layer systems are, instead of having a central system which all systems use to communicate most systems simply refer to each other.

* * *

[\[1\]](#ftnt_ref1)[https://www.baeldung.com/intellij-new-custom-plugin](https://www.google.com/url?q=https://www.baeldung.com/intellij-new-custom-plugin&sa=D&ust=1572892994454000)

[\[2\]](#ftnt_ref2)[https://gist.github.com/aamirsahmad/9a26a8788b86a05cf3af32cdf61ffb7e](https://www.google.com/url?q=https://gist.github.com/aamirsahmad/9a26a8788b86a05cf3af32cdf61ffb7e&sa=D&ust=1572892994455000)