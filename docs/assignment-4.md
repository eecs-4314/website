## Enhancement Proposal

[Presentation Slides](https://docs.google.com/presentation/d/1buw-b96dP13V5S5Omr29G7lot4Sqg7tebFzEuHjZqg8/edit?usp=sharing)


[Report (PDF)](https://drive.google.com/file/d/1ez-hktxxYe0l5NvC8rG-U56qXPmbT2tG/view?usp=sharing)


1 Abstract
==========

This report presents the enhancement of GitFlow based on IntelliJ IDEA software. The report includes an introduction and overview that we want to extend and what the result should look like. And the motivation and the usage of the enhancement also mentioned. We further explore ways, mainly 2 ways of implementing GitFlow into the current Intellij IDEA. The report goes on to discuss the effect each method is going to have on the system as a whole as well as the subsystem. It will also look at the effect each method has on the concurrency and team effect of the program. In addition, risks and limitations of these methods are also discussed. Lastly, there are discussions on all the lessons learned throughout this report and the final verdict on which tool was considered the best for dependency extraction.

2 Introduction and Overview
===========================

IntelliJ IDEA has a feature rich Version Control System (VCS) subsystem that supports popular VCS such as Git, Mercurial, Perforce and so on. Git is arguably the most used VCS in the world which is why IntelliJ IDEA provides superb integration. The VCS subsystem for Git comes with its own user interface that allows users to invoke Git commands via UI components.

![](a4-assets/images/image5.png)

Figure 1: VCS popup in IntelliJ IDEA when enabled

Git allows users to use any branching model or workflow as they like. GitFlow, a popular branching model is one such methodology. Team X noticed that IntelliJ lacks features for GitFlow and does not provide a GUI. Users who would like to use GitFlow within IntelliJ resort to third-party plugins which are not well maintained and are quite bug-ridden. In this report, we propose a GUI that supports GitFlow.

This report also mentions the stakeholders i.e. the entities affected by the enhancement. We also discuss two separate approaches to implement this feature and go over the benefits of each of them. In the Risk and Limitations section we explain the flaws and challenges of the selected approach. Implementing this enhancement requires a lot of work and re-prioritizing agile stories; we discuss the impact in the concurrency and team issues section. Once the enhancement is completed, we propose several testing strategies to ensure the end user gets maximum value from this feature. We conclude this report by giving conclusion and closing remarks regarding lessons learned.

3 Proposed Enhancement and Motivation
=====================================

3.1 GitFlow
-----------

Gitflow Workflow is a Git workflow design that was first published[\[1\]](#ftnt1) and made popular by Vincent Driessen. The Gitflow Workflow defines a strict branching model designed around the project release. This provides a robust framework for managing larger projects.  

Instead of a single master branch, Gitflow uses two branches to record the history of the project. The masterbranch stores the official release history i.e. where the source code of HEAD always reflects a production-ready state, and the developbranch serves as an integration branch for features i.e. where the source code of HEAD always reflects a state with the latest delivered development changes for the next release. The developbranch will contain the complete history of the project, whereas master will contain an abridged version.

When the source code in the develop branch reaches a stable point and is ready to be released, all of the changes should be merged back into master somehow and then tagged with a release number.

GitFlow model uses a variety of supporting branches to aid parallel development between team members, ease tracking of features, prepare for production releases and to assist in quickly fixing live production problems. Unlike the main branches, these branches always have a limited life time, since they will be removed eventually.

The different types of branches are:

*   Feature branches: are used to develop new features for the upcoming or a distant future release.
*   Release branches: support preparation of a new production release.
*   Hotfix branches: may be branched off from the corresponding tag on the master branch that marks the production version for a quick production bugfix.

![](a4-assets/images/image3.png)

Figure 2: GitFlow Workflow

3.2 Motivation
--------------

Due to GitFlow wide adoption[\[2\]](#ftnt2) in the software industry - thanks to its simple yet practical abstraction over Git, we believe GitFlow UI should be shipped with stock IntelliJ.

3.3 Enhancement
---------------

Implement a GitFlow plugin with user interface that exposes GitFlow commands (Appendix 2) as depicted by following mocks.

![](a4-assets/images/image4.png)

Figure 3: GitFlow UI mock A

![](a4-assets/images/image6.png)

Figure 4: GitFlow UI mock B

3.4 Usage
---------

The enhancement will contain the following properties:

*   provide option via GUI to user to initialize a GitFlow repository
*   provide access to GitFlow commands via GUI elements (table 5)
*   display branching structure of tree specific to GitFlow i.e. 2 parallel branches: master and develop in the VCS window group

![](a4-assets/images/image2.png)

Figure 5: GitFlow commands

4 Stakeholders
==============

There are three primary stakeholders and two secondary stakeholders. The three main stakeholders are JetBrains developers, third-party plugin developers, and end users. JetBrains developers are primary stakeholders because they not only needed to write the code but it will benefit them greatly, by reducing the amount of time spent on using Git commands instead of existing GitFlow workflow. The third-party plugin developers also have a large stake since they will face competition from official GitFlow plugin; they also will not have any incentive to keep working on their GitFlow plugins. The last primary stakeholder is the end user, as they are the ones who will benefit from GitFlow UI. The secondary stakeholders are JetBrains and competitors. JetBrains is a stakeholder because it is their product despite it being open source. The competitors of VCS tools such as SourceTree also holds a stake in the success and failure of GitFlow UI and this enhancement would have a large impact on the competitive market.

5 Approaches
============

The gitflow has become more and more popular for software development. It was developed to manage the branching mechanism with a standardised approach. Gitflow also makes management of software development easier because it can keep track of each branch and allow developers to speed up the developing procedure by using this familiar branch structure. In addition, intelliJ IDEA supports Git in Version Control System, and it is always  used to develop a large project. In future, gitflow is very important and efficient for intelliJ development.  The below description includes two different approaches to create gitflow in intelliJ IDEA. 

5.1 Approach A
--------------

We will discuss how to achieve gitflow by extending the functionality of Version Control System in this approach.The gitflow is a git workflow, and its structure is a strict branching model design during the whole project release. Therefore, gitflow is based on Git. We definitely can implement gitflow by standard git commands. What is more, intelliJ IDEA supports invoking Git commands via UI components. Hence, we only need to extend the functionality of Version Control System and GUI which allows users to create a solely gitflow. The commands of gitflow can be implemented by git commands, which is similar to the existing Version Control System of Git. The benefit of this approach is to save more time on gitflow development. On the other side, the disadvantage of this approach is that we have to understand how the Version Control System run, and which is a complicated system.

5.2 Approach B
--------------

Plugins are fully supported by intelliJ IDEA, and Plugins can extend the platform in many ways, so we can create a plugin to implement the gitflow. The tool integration plugin is the best type of plugins for our enhancement because tool integration allows to manipulate the third-party tools and get components directly from IDE without switching contexts. The feature of plugin will contain all necessary commands of gitflow, and all these commands could be visualizable, so users can select the commands from specific UI without typing into the terminal. Moreover, the plugin can display the current status so users can realize which feature branch they are working on. This approach is more scalability than approach A. However, the issue with this approach is that we have to create all functions starting from zero. Developers need to spend more time on development of the plugin.

6 Impact on Subsystems
======================

According to our analysis, plugin subsystem, Version Control System(VCS) and UI subsystem will be impacted by gitflow. Version Control System is an semi-independent system in intelliJ IDEA, which only associates with UI subsystem and Plugin subsystem. If implementing the Approach A, VCS will extend Git functionality to achieve gitflow. Version Control System is provided by a set of bundled plugins, so extending functions of VCS are similar to extend the functions of a plugin. For Approach B, we are going to create a new plugin of gitflow. Therefore, these two approaches are similar and they will impact the same subsystems. Version Control System allows users to clone the Git repository and save those directories locally. These directories will not influence intelliJ IDEA because they do not belong to PSI and File subsystem of intelliJ, and they only interact with VSC. Moreover, UI subsystem will be affected because we have to create more GUI commands and the status bar.

7 Effects on Concurrency & Team issues
======================================

Looking at the approach A, since we are building on top of the Version Control System, it will have no noticeable impact or on the concurrency of the program. However, by building on top of it of the Version Control System, it might lead to confusion to the user and affect their development progress. Git flow has a very specific workflow and if not follow it will lead to conflict and harm the team efficiency. With the git command still available, inconsistent changes can occur.

For approach B, since we are using it as a plugin extension, it will lead to an extra service running, slowing down the program. Also, if it were to be used alongside the VCS system, it might behave in a very hard to predict way since they are both working on the same git based. However, since the two are separate, it is easier to keep track and maintain for both developers and users.

8 Testing Plans
===============

Testing the impact of newly added feature(s) of a large software application on the rest of the application is a core principle of good software development, which ensures high performance and functionality of the said feature and the overall application. We propose a functional and nonfunctional software testing mechanism  to ensure high quality software.  

As part of our functional testing, we plan to use agile methodology to test our feature development throughout it's progressive stages. This includes unit testing,  integration testing, system testing and acceptance testing. When integration testing, we will look to see if GitFlow behaves as expected in conjunction with UI, action, and core subsystems. This integration testing will expose any immediate defects caused between GitFlow and other interacting components. Next, we will be performing a system test to ensure the end-to-end flow is behaving as per requirements. System testing will check to see how IntelliJ IDEA behaves including GitFlow Workflow as part of its feature. Any indirect defects caused by our feature affecting the overall application will be detected here.

The non-functional testing phase includes performance, security, usability and compatibility testing. Our goal here is to ensure performance is not compromised with the added feature. In addition, we must ensure there are no new vulnerabilities are introduced to the overall application by the addition of our feature. When compatibility testing, we will check to see how GitFlow behaves on different platforms such as MacOS, Windows and Linux. Compatibility testing and integration testing work closely together to ensure no defects are caused due to variations in the software’s environment.

Lastly, we will be performing regression testing followed by acceptance testing. In regression testing we will retest every phase again with boundary cases to ensure there minimal bugs at production release. After this step is complete we can move onto acceptance testing where we determine if our requirements are met and whether we are ready for release.

9 Example Use Case
==================

![](a4-assets/images/image1.jpg)

The following is an illustration of how Branches can be made in the proposed change to the system. The user can use a gui tool first to create a master branch. After this the user can create a Develop branch. This develop branch will be used to create and edit any component being built. To build a feature for a component a feature branch can be made. After that feature is completed it can be merged with the develop branch. After all the features for a component are completed in the develop branch a new branch has to be made. This branch is called the release branch which get all the code developed in the develop branch. After that it merges with the master branch and develop branch.

Such a system if implemented in the current project will be very helpful in large scale projects as it provides a very nice and clean method to develop a project.

10 Risks and Limitations of Proposed Enhancement
================================================

Adding a new feature to the intelliJ platform comes with many risks. Firstly, it is entirely possible that through the addition of a feature that will regularly connect to the open internet the feature may inadvertently introduce some security risks and vulnerabilities. Opening any software to the internet can be dangerous, doubly so when a software requires a user to input their credentials to use the software. Adding the gitflow feature will require users to login to their git accounts which further requires sensitive information to pass through the system. These risks may be worth it as cyber security has become heavily invested in and anyone who would be willing to use git through the intellij system should be comfortable using the new gitflow system we propose. Another risk we incur in providing this feature is the risk of falling behind with updates. This feature would now depend on both the main system and a third party, git. If gitflow only depended on the intellij system then it could be updated with the rest of intellij but since it incorporates a third party it now becomes a balancing act of keeping updates for both systems up to date for gitflow.

Limitations within the system also arise. The main limitation will be intellijs very own UI system. We will be required to only use what intellij will be able to provide which could become a limitation. However, as of writing this report we have noted that it may be possible to incorporate gitflow within intellij using it’s UI systems. It is still a possible limitation nonetheless and we will keep our eyes on any problems that may arise in the future.

11 Conclusion
=============

In conclusion we can say that if this feature is added in the existing project it will help all users especially people working on large scale projects. The proposed feature helps create well defined phases of development. For example the develop branch could be named after each phase of the project that way if there is ever a need to go back and see the work done in earlier phases it would be possible to do so.

The software could be released quicker than usual. The project is developed component wise and each component is built under the develop branch with the master branch having the main software. Every component completed could be merged with the master and if all necessary components are merged with the master than the software could be released while further work is being done on other components in other develop branches.

Since the code is split for all components and the component code is split further into feature code the class size is much smaller with less lines of code. Because of this the code looks much cleaner and easily manageable. Also this makes bug fixing much easier. Finding the desired class is much easier as the code is placed in branches that are very well named.  

12 Lessons Learned
==================

Our group learned so much over the past few months. Firstly I’ll speak about this project, we went into this project believing it would be a very easy problem to solve. How hard could it be to come up with an intriguing feature we asked ourselves. We very quickly realized that we had no idea what feature would make a good feature. We deliberated, and finally we came to the conclusion that gitflow would make a great addition to the intellij system. Then we began working and we quickly began to understand the nuances of creating a brand new feature. It’s here that our group learned the most. We learned about how our system will need to touch almost all other systems. We learned about the risks of adding a new feature and we learned that there are many ways to develop and implement a feature.

All in all, this course gave us a great insight into how commercially available products are constructed. Our group will walk away from this semester all the more knowledgeable on how to approach reverse engineering programs, developing system schemas, use case diagrams, and many more extremely helpful tips and tricks in relation to software development. We will certainly remember these four projects for many years as we move forward into out careers as computer scientists/engineers.

Appendix
========

1 Glossary
----------

VCS: Version Control System

* * *

2 GitFlow Commands
------------------

### Initialize

Start using git-flow by initializing it inside an existing git repository:  

git flow init

### Start a new feature

Development of new features starting from the 'develop' branch. Start developing a new feature with:

git flow feature start MYFEATURE

This action creates a new feature branch based on 'develop' and switches to it

### Finish up a feature

Finish the development of a feature. This action performs the following

*   Merges MYFEATURE into 'develop'
*   Removes the feature branch
*   Switches back to 'develop' branch

git flow feature finish MYFEATURE

### Publish a feature

Publish a feature to the remote server so it can be used by other users.

git flow feature publish MYFEATURE

### Getting a published feature

Get a feature published by another user.

git flow feature pull origin MYFEATURE

Alternatively, track a feature on origin by using

git flow feature track MYFEATURE

### Start a release

To start a release, use the git flow release command. It creates a release branch created from the 'develop' branch.

git flow release start RELEASE \[BASE\]

git flow release publish RELEASE

Optionally supply a \[BASE\] commit sha-1 hash to start the release from. The commit must be on the 'develop' branch.

### Finishing a release

Finishing a release is one of the big steps in git branching. It performs several actions:

*   Merges the release branch back into 'master'
*   Tags the release with its name
*   Back-merges the release into 'develop'
*   Removes the release branch

git flow release finish RELEASE

* * *

[\[1\]](#ftnt_ref1)[https://github.com/nvie/gitflow](https://www.google.com/url?q=https://github.com/nvie/gitflow&sa=D&ust=1575313442159000)

[\[2\]](#ftnt_ref2)[https://youtrack.jetbrains.com/issue/IDEA-65491](https://www.google.com/url?q=https://youtrack.jetbrains.com/issue/IDEA-65491&sa=D&ust=1575313442160000)